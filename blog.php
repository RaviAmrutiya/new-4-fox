<!--
Author: W3layouts
Author URL: http://w3layouts.com
-->
<!doctype html>
<html lang="en">


<!-- Mirrored from demo.w3layouts.com/demos_new/template_demo/18-12-2019/eccentricportfolio-liberty-demo_Free/810562296/web/blog.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 17 May 2020 12:07:37 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Eccentric portfolio - personal website template | Multi blog post : W3layouts</title>

    <!-- google fonts -->
    <!-- <link href="../../../../../../../fonts.googleapis.com/css07c1.css?family=Nunito:400,700&amp;display=swap" rel="stylesheet"> -->

    <!-- Template CSS -->
    <link rel="stylesheet" href="assets/css/style-liberty.css">

</head>

<body>
    <!-- <script src='../../../../../../../ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script><script src="../../../../../../../m.servedby-buysellads.com/monetization.js" type="text/javascript"></script> -->
    <script>
        (function() {
            if (typeof _bsa !== 'undefined' && _bsa) {
                // format, zoneKey, segment:value, options
                _bsa.init('flexbar', 'CKYI627U', 'placement:w3layoutscom');
            }
        })();
    </script>
    <script>
        (function() {
            if (typeof _bsa !== 'undefined' && _bsa) {
                // format, zoneKey, segment:value, options
                _bsa.init('fancybar', 'CKYDL2JN', 'placement:demo');
            }
        })();
    </script>
    <script>
        (function() {
            if (typeof _bsa !== 'undefined' && _bsa) {
                // format, zoneKey, segment:value, options
                _bsa.init('stickybox', 'CKYI653J', 'placement:w3layoutscom');
            }
        })();
    </script>
    <!--<script>(function(v,d,o,ai){ai=d.createElement("script");ai.defer=true;ai.async=true;ai.src=v.location.protocol+o;d.head.appendChild(ai);})(window, document, "//a.vdo.ai/core/w3layouts_V2/vdo.ai.js?vdo=34");</script>-->
    <div id="codefund">
        <!-- fallback content -->
    </div>
    <!-- <script src="../../../../../../../codefund.io/properties/441/funder.js" async="async"></script> -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src='https://www.googletagmanager.com/gtag/js?id=UA-149859901-1'></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-149859901-1');
    </script>

    <script>
        window.ga = window.ga || function() {
            (ga.q = ga.q || []).push(arguments)
        };
        ga.l = +new Date;
        ga('create', 'UA-149859901-1', 'demo.w3layouts.com');
        ga('require', 'eventTracker');
        ga('require', 'outboundLinkTracker');
        ga('require', 'urlChangeTracker');
        ga('send', 'pageview');
    </script>
    <!-- <script async src='../../../../../../js/autotrack.js'></script> -->

    <meta name="robots" content="noindex">
    <!-- <body><link rel="stylesheet" href="../../../../../../images/demobar_w3_4thDec2019.css"> -->

    <!-- inner page header -->
    <?php include('header.php');

    $username = "4fox_web_solution";
    $html = file_get_contents("https://www.instagram.com/$username?__a=1");

    $html = json_decode($html, true);
    // for ($i=0; $i < 12; $i++) { 
    //     $pos[$i]= $html['graphql']['user']['edge_owner_to_timeline_media']['edges'][$i]['node']['display_url'];
    //     $a= $html['graphql']['user']['edge_owner_to_timeline_media']['edges'][$i]['node']['edge_media_to_caption']['edges'][0]['node']['text'];
    //     $b = $html['graphql']['user']['edge_owner_to_timeline_media']['edges'][$i]['node']['accessibility_caption'];
    //     // echo '<img src="'.$pos[$i].'" width="100px"  height="100px"><br>';
    //     // print_r($b);'<br>';
    // }   



    ?>
    <!-- //inner page header-->

    <!-- blog with slider -->
    <!-- <section class="blog-slider py-5">
    <div class="container py-lg-4">
        <div class="title-section">
            <h3 class="main-title-w3">Blog Posts</h3>
            <div class="title-line">
            </div>
            <h5>I Write beautiful things and stories.</h5>
        </div>
        <div class="inner-sec-w3layouts mt-md-5 mt-4">
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <div class="content-left-sec">
                        <a href="single.html"><img src="assets/images/1.jpg" class="img img-responsive" alt=""></a>
                        <a href="single.html">
                            <h4 class="mt-4 mb-0">Blog post title 1</h4>
                        </a>
                        <h6 class="mt-sm-2 mt-1">Dec 6th, 2019</h6>
                        <a href="#category" class="category">- Personal</a>
                        <p class="mt-2 mb-0">Donec luctus rhoncus dignissim. Integer blandit mattis arcu, id viverra
                            orci a.</p>
                    </div>
                </div>
                <div class="item">
                    <div class="content-left-sec">
                        <a href="single.html"><img src="assets/images/2.jpg" class="img img-responsive" alt=""></a>
                        <a href="single.html">
                            <h4 class="mt-4 mb-0">Blog post title 2</h4>
                        </a>
                        <h6 class="mt-sm-2 mt-1">Dec 7th, 2019</h6>
                        <a href="#category" class="category">- Personal</a>
                        <p class="mt-2 mb-0">Donec luctus rhoncus dignissim. Integer blandit mattis arcu, id viverra
                            orci a.</p>
                    </div>
                </div>
                <div class="item">
                    <div class="content-left-sec">
                        <a href="single.html"><img src="assets/images/3.jpg" class="img img-responsive" alt=""></a>
                        <a href="single.html">
                            <h4 class="mt-4 mb-0">Blog post title 3</h4>
                        </a>
                        <h6 class="mt-sm-2 mt-1">Dec 8th, 2019</h6>
                        <a href="#category" class="category">- Personal</a>
                        <p class="mt-2 mb-0">Donec luctus rhoncus dignissim. Integer blandit mattis arcu, id viverra
                            orci a.</p>
                    </div>
                </div>
                <div class="item">
                    <div class="content-left-sec">
                        <a href="single.html"><img src="assets/images/4.jpg" class="img img-responsive" alt=""></a>
                        <a href="single.html">
                            <h4 class="mt-4 mb-0">Blog post title 4</h4>
                        </a>
                        <h6 class="mt-sm-2 mt-1">Dec 9th, 2019.</h6>
                        <a href="#category" class="category">Personal</a>
                        <p class="mt-2 mb-0">Donec luctus rhoncus dignissim. Integer blandit mattis arcu, id viverra
                            orci a.</p>
                    </div>
                </div>
                <div class="item">
                    <div class="content-left-sec">
                        <a href="single.html"><img src="assets/images/1.jpg" class="img img-responsive" alt=""></a>
                        <a href="single.html">
                            <h4 class="mt-4 mb-0">Blog post title 5</h4>
                        </a>
                        <h6 class="mt-sm-2 mt-1">Dec 10th, 2019.</h6>
                        <a href="#category" class="category">Personal</a>
                        <p class="mt-2 mb-0">Donec luctus rhoncus dignissim. Integer blandit mattis arcu, id viverra
                            orci a.</p>
                    </div>
                </div>
                <div class="item">
                    <div class="content-left-sec">
                        <a href="single.html"><img src="assets/images/2.jpg" class="img img-responsive" alt=""></a>
                        <a href="single.html">
                            <h4 class="mt-4 mb-0">Blog post title 6</h4>
                        </a>
                        <h6 class="mt-sm-2 mt-1">Dec 11th, 2019.</h6>
                        <a href="#category" class="category">Personal</a>
                        <p class="mt-2 mb-0">Donec luctus rhoncus dignissim. Integer blandit mattis arcu, id viverra
                            orci a.</p>
                    </div>
                </div>
                <div class="item">
                    <div class="content-left-sec">
                        <a href="single.html"><img src="assets/images/3.jpg" class="img img-responsive" alt=""></a>
                        <a href="single.html">
                            <h4 class="mt-4 mb-0">Blog post title 7</h4>
                        </a>
                        <h6 class="mt-sm-2 mt-1">Dec 12th, 2019.</h6>
                        <a href="#category" class="category">Personal</a>
                        <p class="mt-2 mb-0">Donec luctus rhoncus dignissim. Integer blandit mattis arcu, id viverra
                            orci a.</p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section> -->
    <!-- //blog with slider -->

    <div class="display-ad" style="margin: 8px auto; display: block; text-align:center;">
        <!---728x90--->

    </div>
    <!-- blog 3 grids -->
    <section class="content-with-photo-15 py-5">
        <div class="content-photo py-lg-4">
            <div class="container">
                <h3 class="serviceHead">Our Instagram Posts</h3>

                <div class="row">
                    <?php for ($i = 0; $i < 12; $i++) {
                        $pos[$i] = $html['graphql']['user']['edge_owner_to_timeline_media']['edges'][$i]['node']['display_url'];
                        $a = "No Caption";
                        if (!empty($html['graphql']['user']['edge_owner_to_timeline_media']['edges'][$i]['node']['edge_media_to_caption']['edges'])) {
                            $a = $html['graphql']['user']['edge_owner_to_timeline_media']['edges'][$i]['node']['edge_media_to_caption']['edges'][0]['node']['text'];
                        }
                        $b = $html['graphql']['user']['edge_owner_to_timeline_media']['edges'][$i]['node']['accessibility_caption'];
                        $c = explode(".", $b); ?>
                        <div class="col-lg-4 col-md-6 content-left-sec" style="margin-bottom: 58px !important;">
                            <img src="<?php echo $pos[$i]; ?>" class="img img-responsive" alt="" width="700px" height="300px">
                            <a href="single.html">
                                <!-- <h4 class="mt-4 mb-0">Blog post title 1</h4> -->
                            </a>
                            <h6 class="mt-sm-2 mt-1"><?php echo $c[0] ?></h6>
                            <a href="javascript:void(0)" class="category">- Caption</a>
                            <p class="mt-2 mb-0" style="color: black;"><?php echo $a; ?></p>
                        </div>
                    <?php } ?>

                </div>
            </div>
        </div>
    </section>
    <!-- //blog 3 grids -->

    <div class="display-ad" style="margin: 8px auto; display: block; text-align:center;">
        <!---728x90--->
    </div>
    <!-- blog full with image post -->
    <!-- <div class="multi-post py-5">
    <div class="full-width-image-post py-lg-4">
        <div class="container mx-auto">
            <a href="single.html"><img src="assets/images/g1.jpg" class="img-fluid mb-4" alt="full width image" /></a>
            <div class="full-width-image-post-content">
                <a href="single.html">
                    <h4 class="mt-2 mb-0">Full width image Blog post title</h4>
                </a>
                <h6 class="mt-sm-2 mt-1">Dec 6th, 2019</h6>
                <a href="#category" class="category">- Uncategorized</a>
                <p class="mb-0 mt-2">Donec luctus rhoncus dignissim. Integer blandit mattis arcu, id viverra orci
                    condimentum a.
                    Quisque nibh tortor, mollis placerat semper ac, auctor quis mauris. Praesent dignissim sed
                    magna eu urna consectetur, at pretium nisi aliquet. Sed vestibulum malesuada semper.</p>
                <a href="single.html" class="btn-primary btn mt-md-5 mt-4 primary-btn-style">Read More</a>
            </div>
        </div>
        <div class="text-image-post">
        </div>
    </div>
</div> -->
    <!-- //blog full with image post -->

    <div class="display-ad" style="margin: 8px auto; display: block; text-align:center;">
        <!---728x90--->
    </div>
    <!-- image and text blog grids -->
    <!-- <section class="content-with-photo-15 py-5">
    <div class="content-photo py-lg-4">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 content-left-sec">
                    <a href="single.html"><img src="assets/images/1.jpg" class="img img-responsive" alt=""></a>
                </div>
                <div class="col-lg-6 content-left-sec">
                    <a href="single.html">
                        <h4 class="mt-4 mb-0">Blog Post title</h4>
                    </a>
                    <h6 class="mt-sm-2 mt-1">Dec 6th, 2019.</h6>
                    <a href="#category" class="category">Portfolio</a>
                    <p class="mt-2 mb-0">Donec luctus rhoncus dignissim. Integer blandit mattis arcu, id viverra
                        dolor. id viverra orci condimentum a. Donec luctus rhoncus dignissim. Integer blandit mattis
                        arcu,
                        Quisque nibh tortor, mollis placerat semper ac, auctor quis mauris. Praesent dignissim sed
                        magna eu urna consectetur, at pretium nisi aliquet. Sed malesuada semper</p>
                    <a href="single.html" class="btn-primary btn mt-lg-5 mt-4 primary-btn-style">Read More</a>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-lg-6 content-left-sec info-order">
                    <a href="single.html">
                        <h4 class="mt-4 mb-0">Blog post title</h4>
                    </a>
                    <h6 class="mt-sm-2 mt-1">Dec 6th, 2019.</h6>
                    <a href="#category" class="category">Portfolio</a>
                    <p class="mt-2 mb-0">Donec luctus rhoncus dignissim. Integer blandit mattis arcu, id viverra
                        dolor. id viverra orci condimentum a. Donec luctus rhoncus dignissim. Integer blandit mattis
                        arcu,
                        Quisque nibh tortor, mollis placerat semper ac, auctor quis mauris. Praesent dignissim sed
                        magna eu urna consectetur, at pretium nisi aliquet. Sed malesuada semper</p>
                    <a href="single.html" class="btn-primary btn mt-lg-5 mt-4 primary-btn-style">Read More</a>
                </div>
                <div class="col-lg-6 content-left-sec img-order">
                    <a href="single.html"><img src="assets/images/4.jpg" class="img img-responsive" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</section> -->
    <!-- image and text blog grids -->

    <!-- Footer -->
    <?php include('footer.php'); ?>
    <!-- //Footer -->

    <!-- move top -->
    <button onclick="topFunction()" id="movetop" class="bg-primary" title="Go to top">
        <span class="fa fa-angle-up"></span>
    </button>
    <script>
        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {
            scrollFunction()
        };

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                document.getElementById("movetop").style.display = "block";
            } else {
                document.getElementById("movetop").style.display = "none";
            }
        }

        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    </script>
    <!-- /move top -->

    <!-- common jquery -->
    <script src="assets/js/jquery-3.3.1.min.js"></script>
    <!-- //common jquery -->

    <!-- disable body scroll which navbar is in active -->
    <script>
        $(function() {
            $('.navbar-toggler').click(function() {
                $('body').toggleClass('noscroll');
            })
        });
    </script>
    <!-- disable body scroll which navbar is in active -->

    <!-- for blog carousel slider -->
    <script src="assets/js/owl.carousel1.js"></script>
    <script>
        $(document).ready(function() {
            var owl = $('.owl-carousel');
            owl.owlCarousel({
                stagePadding: 20,
                margin: 15,
                nav: false,
                loop: false,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 2
                    },
                    1000: {
                        items: 3
                    }
                }
            })
        })
    </script>
    <!-- //for blog carousel slider -->

    <!--  bootstrap js -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!--  //bootstrap js -->

</body>

<!-- Mirrored from demo.w3layouts.com/demos_new/template_demo/18-12-2019/eccentricportfolio-liberty-demo_Free/810562296/web/blog.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 17 May 2020 12:07:38 GMT -->

</html>