<section class="w3l-footers-1">
	<div class="footer bg-secondary">
		<div class="container">
			<div class="footer-content">
				<div class="row">
					<div class="col-lg-6">
						<h3> <a href="index.php"><b>4FOX WEB SOLUTION</b></a>
							<p class="mb-3">Helping clients for thire growth and providing services as per thire requirement.</p>
							<h5 style="color: #29a19c;">Trusted by <span>500+ People</span> </h5>
							<div class="icon-social mt-4">

								<a href="https://www.instagram.com/4fox_web_solution/" class="button-footr" target="_blank">
									<span class="fa mx-2 fa-instagram"></span>
								</a>
								<a href="https://www.facebook.com/" class="button-footr" target="_blank">
									<span class="fa mx-2 fa-facebook"></span>
								</a>
								<a href="https://twitter.com/login?lang=en" class="button-footr" target="_blank" <span class="fa mx-2 fa-twitter"></span>
									<span class="fa mx-2 fa-twitter"></span>
								</a>
								<a href="https://www.linkedin.com/uas/login" class="button-footr" target="_blank">
									<span class="fa mx-2 fa-linkedin-square"></span>
								</a>
								<a href="#" class="button-footr">
									<span class="fa mx-2 fa-google-plus"></span>
								</a>

							</div>
					</div>
					<!-- <div class="col-lg-4">
						<div class="links-wthree d-flex">

							<ul class="list-info-wthree ml-5">
								<li style="margin-bottom: 7px;">
									<a href="index.php"><span class="fa fa-angle-double-right" aria-hidden="true"></span>
										Home
									</a>
								</li>
								<li style="margin-bottom: 7px;">
									<a href="about.php"><span class="fa fa-angle-double-right" aria-hidden="true"></span>
										About
									</a>
								</li>
								<li style="margin-bottom: 7px;">
									<a href="services.php"><span class="fa fa-angle-double-right" aria-hidden="true"></span>
										Services
									</a>
								</li>
								<li style="margin-bottom: 7px;">
									<a href="Portfolio.php"><span class="fa fa-angle-double-right" aria-hidden="true"></span>
										Portfolio
									</a>
								</li>
								<li style="margin-bottom: 7px;">
									<a href="Blog.php"><span class="fa fa-angle-double-right" aria-hidden="true"></span>
										Blog
									</a>
								</li>

								<li style="margin-bottom: 7px;">
									<a href="contact.html"><span class="fa fa-angle-double-right" aria-hidden="true"></span>
										Contact Us
									</a>
								</li>
							</ul>
						</div>
					</div> -->

					<div class="col-lg-6 mt-4">
						<p><span class="fa fa-map-marker"></span>180,Shakti Chamber-1,behind dena bank 8-A,N.H,morbi-2 363642 .<span>Gujarat,india..</span></p>
						<a href="tel:+918155883237">
							<p class="phone"><span class="fa fa-phone"></span> +91-8155883237 </p>
						</a>

						<p><span class="fa fa-envelope" style="margin-right: 10px;"></span><a href="mailto:info@4foxwebsolution.com">info@4foxwebsolution.com</a></p>
						<p><span class="fa fa-envelope" style="margin-right: 10px;"></span><a href="mailto:4foxwebsolution@gmail.com">4foxwebsolution@gmail.com</a></p>

					</div>
				</div><br>
				<div class="title-line"></div>

				<div class="row">

				<p class="text-lg-left text-center" style="margin-left: 17px;margin-top: 10px;">© 2019 4FOX WEB SOLUTION. All rights reserved
                    </p>
				</div>
				<!-- <div class="row">
					<div class="col-lg-6 footer-left">
						<p class="m-0">© Copyright 2020 Eccentric Portfolio. Design by <a href="https://w3layouts.com/">W3layouts</a></p>
					</div>
					<div class="col-lg-4 footer-right text-lg-right text-center mt-lg-0 mt-3">
						<ul class="social m-0 p-0">
							<li><a href="#facebook"><span class="fa fa-facebook"></span></a></li>
							<li><a href="#linkedin"><span class="fa fa-linkedin"></span></a></li>
							<li><a href="#instagram"><span class="fa fa-instagram"></span></a></li>
							<li><a href="#twitter"><span class="fa fa-twitter"></span></a></li>
						</ul>
					</div>
				</div> -->
			</div>
		</div>
	</div>
</section>