<!--
Author: W3layouts
Author URL: http://w3layouts.com
-->
<!doctype html>
<html lang="en">


<!-- Mirrored from demo.w3layouts.com/demos_new/template_demo/18-12-2019/eccentricportfolio-liberty-demo_Free/810562296/web/contact.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 17 May 2020 12:07:40 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Eccentric portfolio - personal website template | Contact : W3layouts</title>

  <!-- google fonts -->
  <!-- <link href="../../../../../../../fonts.googleapis.com/css07c1.css?family=Nunito:400,700&amp;display=swap" rel="stylesheet"> -->

  <!-- Template CSS -->
  <link rel="stylesheet" href="assets/css/style-liberty.css">

</head>

<body>
  <!-- <script src='../../../../../../../ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script><script src="../../../../../../../m.servedby-buysellads.com/monetization.js" type="text/javascript"></script> -->
  <script>
    (function() {
      if (typeof _bsa !== 'undefined' && _bsa) {
        // format, zoneKey, segment:value, options
        _bsa.init('flexbar', 'CKYI627U', 'placement:w3layoutscom');
      }
    })();
  </script>
  <script>
    (function() {
      if (typeof _bsa !== 'undefined' && _bsa) {
        // format, zoneKey, segment:value, options
        _bsa.init('fancybar', 'CKYDL2JN', 'placement:demo');
      }
    })();
  </script>
  <script>
    (function() {
      if (typeof _bsa !== 'undefined' && _bsa) {
        // format, zoneKey, segment:value, options
        _bsa.init('stickybox', 'CKYI653J', 'placement:w3layoutscom');
      }
    })();
  </script>
  <!--<script>(function(v,d,o,ai){ai=d.createElement("script");ai.defer=true;ai.async=true;ai.src=v.location.protocol+o;d.head.appendChild(ai);})(window, document, "//a.vdo.ai/core/w3layouts_V2/vdo.ai.js?vdo=34");</script>-->
  <div id="codefund">
    <!-- fallback content -->
  </div>
  <!-- <script src="../../../../../../../codefund.io/properties/441/funder.js" async="async"></script> -->

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src='https://www.googletagmanager.com/gtag/js?id=UA-149859901-1'></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-149859901-1');
  </script>

  <script>
    window.ga = window.ga || function() {
      (ga.q = ga.q || []).push(arguments)
    };
    ga.l = +new Date;
    ga('create', 'UA-149859901-1', 'demo.w3layouts.com');
    ga('require', 'eventTracker');
    ga('require', 'outboundLinkTracker');
    ga('require', 'urlChangeTracker');
    ga('send', 'pageview');
  </script>
  <!-- <script async src='../../../../../../js/autotrack.js'></script> -->

  <meta name="robots" content="noindex">
  <!-- <body><link rel="stylesheet" href="../../../../../../images/demobar_w3_4thDec2019.css"> -->
  <!-- Demo bar start -->


  <!-- inner page header -->
  <?php include('header.php'); ?>
  <!-- //inner page header-->

  <!-- contact form -->
  <section class="w3l-contacts-12 py-5">
    <div class="container py-md-3">
      <div class="contacts12-main">
        <div class="title-section">
          <h3 class="main-title-w3 mb-md-5 mb-4">Want to get in touch? <br>Find us on
            <a href="tel:+918155883237" class="text-primary">phone</a>,
            <a href="mailto:info@4foxwebsolution.com" class="text-primary">email</a>,
            <a href="#twitter" class="text-primary">twitter</a>
            or <a href="#linkedin" class="text-primary">linkedin</a>.</h3>
        </div>
        <form action="https://sendmail.w3layouts.com/submitForm" method="post" class="">
          <div class="main-input">
            <input type="text" name="w3lName" placeholder="Enter your name" class="contact-input" required="" />
            <input type="email" name="w3lSender" placeholder="Enter your mail" class="contact-input" required="" />
            <input type="number" name="w3lPhone" placeholder="Your phone number" class="contact-input" required="" />
            <textarea class="contact-textarea contact-input" name="w3lMessage" placeholder="Enter your message" required=""></textarea>
          </div>
          <div class="text-right">
            <button class="btn-primary btn primary-btn-style">Send</button>
          </div>
        </form>
      
      </div>
    
    </div>
    <div class="container py-md-3">
    <div class="map-w3pvt mt-5">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3677.712453981387!2d70.86626591455065!3d22.813116285059564!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39598dc6aef5cb1f%3A0x3a2423bf3e26806b!2s4Fox+Web+solution!5e0!3m2!1sen!2sin!4v1565253761690!5m2!1sen!2sin" allowfullscreen width="100%" height="400px"></iframe>
      </div>
    </div>

  </section>
  <!-- //contact form -->

  <div class="display-ad" style="margin: 8px auto; display: block; text-align:center;">
    <!---728x90--->

  </div>
  <!-- Footer -->
  <section class="w3l-footers-1">
    <?php include('footer.php'); ?>
  </section>
  <!-- //Footer -->

  <!-- move top -->
  <button onclick="topFunction()" id="movetop" class="bg-primary" title="Go to top">
    <span class="fa fa-angle-up"></span>
  </button>
  <script>
    // When the user scrolls down 20px from the top of the document, show the button
    window.onscroll = function() {
      scrollFunction()
    };

    function scrollFunction() {
      if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("movetop").style.display = "block";
      } else {
        document.getElementById("movetop").style.display = "none";
      }
    }

    // When the user clicks on the button, scroll to the top of the document
    function topFunction() {
      document.body.scrollTop = 0;
      document.documentElement.scrollTop = 0;
    }
  </script>
  <!-- /move top -->

  <!-- common jquery -->
  <script src="assets/js/jquery-3.3.1.min.js"></script>
  <!-- //common jquery -->

  <!-- disable body scroll which navbar is in active -->
  <script>
    $(function() {
      $('.navbar-toggler').click(function() {
        $('body').toggleClass('noscroll');
      })
    });
  </script>
  <!-- disable body scroll which navbar is in active -->

  <!--  bootstrap js -->
  <script src="assets/js/bootstrap.min.js"></script>
  <!--  //bootstrap js -->

</body>

<!-- Mirrored from demo.w3layouts.com/demos_new/template_demo/18-12-2019/eccentricportfolio-liberty-demo_Free/810562296/web/contact.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 17 May 2020 12:07:40 GMT -->

</html>