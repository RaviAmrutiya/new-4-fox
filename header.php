<!-- header -->
<div class="inner-banner">
    <div class="w3l-header inner-w3l-header" id="home">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-dark pl-0 pr-0">
                <a class="navbar-brand m-0 text-primary" href=""><span class="fa fa-desktop middle"></span> 4FOX WEB SOLUTION </a>
                </a>
                <!-- <span class="logo">portfolio</span>-->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item mr-lg-4">
                            <a class="nav-link pl-0 pr-0 font-weight-bold" href="index.php">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item  mr-lg-4">
                            <a class="nav-link pl-0 pr-0 font-weight-bold" href="about.php">About <span class="sr-only">(current)</span></a>
                        </li>
                        <!-- <li class="nav-item dropdown mr-lg-3">
                            <a class="nav-link dropdown-toggle font-weight-bold" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                About</a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item font-weight-bold" href="about.html">About me</a>
                                <a class="dropdown-item font-weight-bold" href="portfolio.html">Portfolio</a>
                                <a class="dropdown-item font-weight-bold" href="resume.html">My Resume</a>
                                <a class="dropdown-item font-weight-bold" href="timeline.html">Timeline</a>
                            </div>
                        </li> -->
                        <li class="nav-item  mr-lg-4">
                            <a class="nav-link pl-0 pr-0 font-weight-bold" href="services.php">Services</a>
                        </li>
                        <li class="nav-item  mr-lg-4">
                            <a class="nav-link pl-0 pr-0 font-weight-bold" href="portfolio.php">Portfolio</a>
                        </li>
                        <li class="nav-item  mr-lg-4">
                            <a class="nav-link pl-0 pr-0 font-weight-bold" href="blog.php">Blog</a>
                        </li>
                        <!-- <li class="nav-item dropdown mr-lg-3">
                            <a class="nav-link dropdown-toggle font-weight-bold" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Portfolio</a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item font-weight-bold" href="blog1.html">Image project</a>
                                <a class="dropdown-item font-weight-bold" href="blog2.html">Video project</a>
                                <a class="dropdown-item font-weight-bold" href="blog3.html">Sound project</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown mr-lg-3">
                            <a class="nav-link dropdown-toggle font-weight-bold" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Blog</a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item font-weight-bold" href="blog.html">Multi blog post</a>
                                <a class="dropdown-item font-weight-bold" href="single.html">Single post</a>
                            </div>
                        </li> -->
                        <li class="nav-item">
                            <a class="nav-link pl-0 pr-0 font-weight-bold" href="contact.php">Contact</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>

</div>

<!-- //header -->