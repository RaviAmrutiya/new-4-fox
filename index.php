<!--
Author: W3layouts
Author URL: http://w3layouts.com
-->
<!doctype html>
<html lang="en">


<!-- Mirrored from demo.w3layouts.com/demos_new/template_demo/18-12-2019/eccentricportfolio-liberty-demo_Free/810562296/web/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 17 May 2020 12:07:18 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Eccentric portfolio - personal website template | Home : W3layouts</title>

  <!-- google fonts -->
  <!-- <link href="../../../../../../../fonts.googleapis.com/css07c1.css?family=Nunito:400,700&amp;display=swap" rel="stylesheet"> -->

  <!-- Template CSS -->
  <link rel="stylesheet" href="assets/css/style-liberty.css">

</head>

<body>
  <!-- <script src='../../../../../../../ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script> -->
  <!-- <script src="../../../../../../../m.servedby-buysellads.com/monetization.js" type="text/javascript"></script> -->
  <script>
    (function() {
      if (typeof _bsa !== 'undefined' && _bsa) {
        // format, zoneKey, segment:value, options
        _bsa.init('flexbar', 'CKYI627U', 'placement:w3layoutscom');
      }
    })();
  </script>
  <script>
    (function() {
      if (typeof _bsa !== 'undefined' && _bsa) {
        // format, zoneKey, segment:value, options
        _bsa.init('fancybar', 'CKYDL2JN', 'placement:demo');
      }
    })();
  </script>
  <script>
    (function() {
      if (typeof _bsa !== 'undefined' && _bsa) {
        // format, zoneKey, segment:value, options
        _bsa.init('stickybox', 'CKYI653J', 'placement:w3layoutscom');
      }
    })();
  </script>
  <!--<script>(function(v,d,o,ai){ai=d.createElement("script");ai.defer=true;ai.async=true;ai.src=v.location.protocol+o;d.head.appendChild(ai);})(window, document, "//a.vdo.ai/core/w3layouts_V2/vdo.ai.js?vdo=34");</script>-->
  <div id="codefund">
    <!-- fallback content -->
  </div>
  <!-- <script src="../../../../../../../codefund.io/properties/441/funder.js" async="async"></script> -->

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src='https://www.googletagmanager.com/gtag/js?id=UA-149859901-1'></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-149859901-1');
  </script>

  <script>
    window.ga = window.ga || function() {
      (ga.q = ga.q || []).push(arguments)
    };
    ga.l = +new Date;
    ga('create', 'UA-149859901-1', 'demo.w3layouts.com');
    ga('require', 'eventTracker');
    ga('require', 'outboundLinkTracker');
    ga('require', 'urlChangeTracker');
    ga('send', 'pageview');
  </script>
  <!-- <script async src='../../../../../../js/autotrack.js'></script> -->

  <meta name="robots" content="noindex">

  <body>
    <!-- <link rel="stylesheet" href="../../../../../../images/demobar_w3_4thDec2019.css"> -->


    <!-- include header  -->

    <div class="w3l-header" id="home">
      <div class="container">
        <nav class="navbar navbar-expand-lg navbar-dark pl-0 pr-0">
          <a class="navbar-brand m-0 text-primary" href="index.php"><span class="fa fa-desktop middle"></span> 4FOX WEB SOLUTION </a>
          <!-- <span class="logo">portfolio</span>-->
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item active mr-lg-4">
                <a class="nav-link pl-0 pr-0 font-weight-bold" href="index.php">Home <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item  mr-lg-4">
                <a class="nav-link pl-0 pr-0 font-weight-bold" href="about.php">About <span class="sr-only">(current)</span></a>
              </li>
              <!-- <li class="nav-item dropdown mr-lg-3">
                        <a class="nav-link dropdown-toggle font-weight-bold" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            About</a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item font-weight-bold" href="about.html">About me</a>
                            <a class="dropdown-item font-weight-bold" href="portfolio.html">Portfolio</a>
                            <a class="dropdown-item font-weight-bold" href="resume.html">My Resume</a>
                            <a class="dropdown-item font-weight-bold" href="timeline.html">Timeline</a>
                        </div>
                    </li> -->
              <li class="nav-item mr-lg-4">
                <a class="nav-link pl-0 pr-0 font-weight-bold" href="services.php">Services</a>
              </li>
              <li class="nav-item  mr-lg-4">
                <a class="nav-link pl-0 pr-0 font-weight-bold" href="portfolio.php">Portfolio</a>
              </li>
              <li class="nav-item  mr-lg-4">
                <a class="nav-link pl-0 pr-0 font-weight-bold" href="blog.php">Blog</a>
              </li>
              <!-- <li class="nav-item dropdown mr-lg-3">
                <a class="nav-link dropdown-toggle font-weight-bold" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Portfolio</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item font-weight-bold" href="blog1.html">Image project</a>
                  <a class="dropdown-item font-weight-bold" href="blog2.html">Video project</a>
                  <a class="dropdown-item font-weight-bold" href="blog3.html">Sound project</a>
                </div>
              </li>
              <li class="nav-item dropdown mr-lg-3">
                <a class="nav-link dropdown-toggle font-weight-bold" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Blog</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item font-weight-bold" href="blog.html">Multi blog post</a>
                  <a class="dropdown-item font-weight-bold" href="single.html">Single post</a>
                </div>
              </li> -->
              <li class="nav-item">
                <a class="nav-link pl-0 pr-0 font-weight-bold" href="contact.php">Contact</a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </div>

    <!-- banner slider -->
    <div class="w3l-banner-slider">
      <div class="wrapper-container">
        <!-- Container for all sliders, and pagination -->
        <main class="sliders-container">
          <!-- Here will be injected sliders for images, numbers, titles and links -->

          <!-- Simple pagination for the slider -->
          <ul class="pagination">
            <li class="pagination__item"><a class="pagination__button"></a></li>
            <li class="pagination__item"><a class="pagination__button"></a></li>
            <li class="pagination__item"><a class="pagination__button"></a></li>
            <li class="pagination__item"><a class="pagination__button"></a></li>
          </ul>
          <div class="social">
            <a class="" href="#facebook"><span class="fa fa-facebook"></span></a>
            <a class="" href="#twitter"><span class="fa fa-twitter"></span></a>
            <a class="" href="#instagram"><span class="fa fa-instagram"></span></a>
          </div>
        </main>
      </div>
    </div>
    <!-- //banner slider -->

    <!-- w3l-content-photo-5 -->
    <div class="w3l-content-photo-5 py-5">
      <div class="content-main py-lg-5 py-md-3">
        <div class="container">
          <div class="row">
            <div class="col-lg-7 content-left">
              <h3>GREAT COMPANIES ARE BUILT IN OFFICE , WITH HARD WORK PUT BY A TEAM.</h3>
              <p class="mb-0">4fox is working towards digital india, providing the best services to clients. Creating the awarness of this digital era to the people
                Nowdays it is very important to take your bussniess online so that you can spread it to the every corner of the world and we are glad that we are part of this and helping people to grow thire bussniess.</p>
              <a href="blog.php" class="primary-btn-style btn-primary btn mt-lg-5 mt-4">View Our Blog</a>
            </div>
            <div class="col-lg-5 content-photo mt-lg-0 mt-sm-5 mb-md-0 mb-4">
              <a href="#"><img src="assets/images/team.jpeg" class="img-fluid" alt="content-photo"></a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- //w3l-content-photo-5 -->

    <div class="display-ad" style="margin: 8px auto; display: block; text-align:center;">
      <!---728x90--->

    </div>
    <!-- gallery-7 -->
    <section class="w3l-gallery-7 bg-light py-5">
      <div class="gallery-7_sur py-lg-5">
        <div class="container">
          <div id="container">
            <div class="galleryContainer">
              <div class="gallery">
                <input type="radio" name="controls" id="c1" checked><img class="galleryImage img-fluid" id="i1" src="assets/images/raksha.jpg" class="img" alt="">
                <input type="radio" name="controls" id="c2"><img class="galleryImage img-fluid" id="i2" src="assets/images/design.jpg" class="img" alt="">
                <input type="radio" name="controls" id="c3"><img class="galleryImage img-fluid" id="i3" src="assets/images/gandhi.jpg" class="img" alt="">
                <input type="radio" name="controls" id="c4"><img class="galleryImage img-fluid" id="i4" src="assets/images/independence.jpg" class="img" alt="">
                <input type="radio" name="controls" id="c5"><img class="galleryImage img-fluid" id="i5" src="assets/images/tourism.jpg" class="img" alt="">
                <input type="radio" name="controls" id="c6"><img class="galleryImage img-fluid" id="i6" src="assets/images/engineer.jpg" class="img" alt="">
                <input type="radio" name="controls" id="c7"><img class="galleryImage img-fluid" id="i7" src="assets/images/build.jpg" class="img" alt="">

              </div>
              <div class="thumbnails">
                <label class="thumbnailImage" for="c1"><img src="assets/images/raksha.jpg" class="img img-fluid" alt=""></label>
                <label class="thumbnailImage" for="c2"><img src="assets/images/design.jpg" class="img img-fluid" alt=""></label>
                <label class="thumbnailImage" for="c3"><img src="assets/images/gandhi.jpg" class="img img-fluid" alt=""></label>
                <label class="thumbnailImage" for="c4"><img src="assets/images/independence.jpg" class="img img-fluid" alt=""></label>
                <label class="thumbnailImage" for="c5"><img src="assets/images/tourism.jpg" class="img img-fluid" alt=""></label>
                <label class="thumbnailImage" for="c6"><img src="assets/images/engineer.jpg" class="img img-fluid" alt=""></label>
                <label class="thumbnailImage" for="c7"><img src="assets/images/build.jpg" class="img img-fluid" alt=""></label>


              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- //gallery-7 -->

    <div class="display-ad" style="margin: 8px auto; display: block; text-align:center;">
      <!---728x90--->
    </div>
    <!-- iphone home block -->
    <!-- <section class="w3l-blog py-5">
      <div class="container py-lg-5">
        <div class="row">
          <div class="col-lg-3 col-6 pr-md-3 pr-2">
            <div class="img-block">
              <a href="#img">
                <img src="assets/images/iphone1.png" class="img-fluid" alt="image" />
              </a>
            </div>
          </div>
          <div class="col-lg-3 col-6 pl-md-3 pl-2">
            <div class="img-block">
              <a href="#img">
                <img src="assets/images/iphone2.png" class="img-fluid" alt="image" />
              </a>
            </div>
          </div>
          <div class="col-lg-6 mt-lg-0 mt-md-5 mt-4">
            <h3>Sed volutpat eget dui ut tempus init bibendum nunc.</h3>
            <h5 class="mt-3">Fusce fringilla tincidunt laoreet volutpat cras varius sit suscipit.</h5>
            <p class="mt-4 mb-0"> Sed in metus libero. Sed volutpat eget dui ut tempus. Fusce fringilla tincidunt laoreet
              Morbi ac metus vitae diam scelerisque malesuada eget eu mauris. Cras varius lorem ac velit pharetra,
              non scelerisque mi vulputate. Phasellus bibendum suscipit nunc, non semper erat iaculis in. Nulla
              volutpat porttitor magna vel euismod. Aenean sit amet diam nec sem
              amet metus.</p>
            <a href="blog1.html" class="primary-btn-style btn-primary btn mt-lg-5 mt-4">View my project</a>
          </div>
        </div>
      </div>
    </section> -->
    <!-- //iphone home block -->

    <div class="display-ad" style="margin: 8px auto; display: block; text-align:center;">
      <!---728x90--->
    </div>
    <!-- testimonial -->
    <section class="w3l-quote-main">
      <div class="quote py-5">
        <div class="container py-lg-5">
          <div class="row">
            <div class="col-lg-12">
              <h4>
                <center>Helping clients for thire growth and providing services.</center>

              </h4>
            </div>
            <!-- <div class="col-lg-3 mt-lg-0 mt-3 text-lg-center tablet-grid">
              <img src="assets/images/client2.jpg" alt="" class="img-fluid" />
              <div>
                <h6 class="mb-0 mt-lg-3">Johnson smith</h6>
                <h5 class="mt-1">- UI/UX Designer </h5>
              </div>
            </div> -->
          </div>
        </div>
      </div>
    </section>
    <!-- //testimonial -->

    <!-- services block 2 -->
    <div class="w3l-services-block py-5" id="classes">
      <div class="container py-lg-5 py-md-3">
        <div class="services-block_grids_bottom">
          <div class="row">
            <div class="col-lg-4 col-md-6 service_grid_btm_left">
              <div class="service_grid_btm_left1">
                <div class="service_grid_btm_left2">
                  <span class="fa fa-globe text-primary" aria-hidden="true"></span>
                  <h5>OUR VISION</h5>
                  <p>Our vision is to take every small and big bussniess online and spread them as much as possible.</p>
                </div>
                <!-- <img src="assets/images/sarah.jpg" alt=" " class="img-fluid" /> -->
              </div>
            </div>
            <div class="col-lg-4 col-md-6 service_grid_btm_left mt-md-0 mt-sm-5 mt-4">
              <div class="service_grid_btm_left1">
                <div class="service_grid_btm_left2">
                  <span class="fa fa-bullseye text-primary" aria-hidden="true"></span>
                  <h5>OUR MISSION</h5>
                  <p>Our mission is to be the leading IT company with good reputation among clients.</p>
                </div>
                <!-- <img src="assets/images/bench.jpg" alt=" " class="img-fluid" /> -->
              </div>
            </div>
            <div class="col-lg-4 col-md-6 service_grid_btm_left mt-lg-0 mt-sm-5 mt-4">
              <div class="service_grid_btm_left1">
                <div class="service_grid_btm_left2">
                  <span class="fa fa-eye-slash text-primary" aria-hidden="true"></span>
                  <h5>OUR GOAL</h5>
                  <p>Our goal is to provide clients with services and be supportive towards thire requirements.</p>
                </div>
                <!-- <img src="assets/images/alexandra.jpg" alt=" " class="img-fluid" /> -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- // services block2 -->

    <!-- home page blog-->
    <!-- <section class="w3l-blog py-5 bg-light">
      <div class="container py-lg-5 py-md-3">
        <div class="row">
          <div class="col-lg-5">
            <h3>Sed volutpat eget dui ut tempus init.</h3>
            <h5 class="mt-3">Fusce fringilla tincidunt laoreet volutpat cras varius sit </h5>
            <p class="mt-4"> Sed in metus libero. Sed volutpat eget dui ut tempus. Fusce fringilla tincidunt laoreet
              Morbi ac metus vitae diam scelerisque malesuada eget eu mauris. Cras varius lorem ac velit pharetra,
              non scelerisque mi vulputate. Phasellus bibendum suscipit nunc, non semper erat iaculis in. Nulla
              volutpat porttitor magna vel euismod. Aenean sit amet diam nec sem
              amet metus.</p>
          </div>
          <div class="col-lg-7 mt-lg-0 mt-4">
            <div class="img-block">
              <a href="single.html">
                <img src="assets/images/g1.jpg" class="img-fluid" alt="image" />
                <span>Modern Ecommerce Website Design</span>
              </a>
            </div>
            <div class="img-block mt-3">
              <a href="single.html"> <img src="assets/images/g2.jpg" class="img-fluid" alt="image" />
                <span>Personalized Portfolio work</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section> -->
    <!-- //home page blog-->

    <!-- freelance hire me -->
    <!-- <section class="w3l-grid-quote py-5">
    <div class="container py-lg-3">
        <h6>I'am available for freelance projects.</h6>
        <h3>Let's work together indeed!</h3>
        <a href="contact.html" class="secondary-btn-style btn-secondary btn">Get quotes</a>
        <a href="contact.html" class="btn btn-style text-primary ml-2">Hire me</a>
    </div>
</section> -->
    <!-- //freelance hire me -->

    <!-- Footer -->
    <?php include('footer.php'); ?>
    <!-- //Footer -->

    <!-- move top -->
    <button onclick="topFunction()" id="movetop" class="bg-primary" title="Go to top">
      <span class="fa fa-angle-up"></span>
    </button>
    <script>
      // When the user scrolls down 20px from the top of the document, show the button
      window.onscroll = function() {
        scrollFunction()
      };

      function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
          document.getElementById("movetop").style.display = "block";
        } else {
          document.getElementById("movetop").style.display = "none";
        }
      }

      // When the user clicks on the button, scroll to the top of the document
      function topFunction() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
      }
    </script>
    <!-- /move top -->

    <!-- common jquery -->
    <script src="assets/js/jquery-3.3.1.min.js"></script>
    <!-- //common jquery -->

    <!-- // for banner slider -->
    <script src="assets/js/momentum-slider.min.js"></script>
    <script>
      (function() {

        var slidersContainer = document.querySelector('.sliders-container');

        // Initializing the numbers slider
        var msNumbers = new MomentumSlider({
          el: slidersContainer,
          cssClass: 'ms--numbers',
          range: [1, 4],
          rangeContent: function(i) {
            return '0' + i;
          },
          style: {
            transform: [{
              scale: [0.4, 1]
            }],
            opacity: [0, 1]
          },

          interactive: false
        });


        // Initializing the titles slider
        var titles = [
          'Web Development ',
          'App Development',
          'SAAS Development',
          'Cross -- platform  Development'
        ];

        var msTitles = new MomentumSlider({
          el: slidersContainer,
          cssClass: 'ms--titles',
          range: [0, 3],
          rangeContent: function(i) {
            return '<h3>' + titles[i] + '</h3>';
          },
          vertical: true,
          reverse: true,
          style: {
            opacity: [0, 1]
          },

          interactive: false
        });


        // Initializing the links slider
        var msLinks = new MomentumSlider({
          el: slidersContainer,
          cssClass: 'ms--links',
          range: [0, 3],
          rangeContent: function() {
            return '<a href="services.php" class="ms-slide__link btn">View More</a>';
          },
          vertical: true,
          interactive: false
        });


        // Get pagination items
        var pagination = document.querySelector('.pagination');
        var paginationItems = [].slice.call(pagination.children);

        // Initializing the images slider
        var msImages = new MomentumSlider({
          // Element to append the slider
          el: slidersContainer,
          // CSS class to reference the slider
          cssClass: 'ms--images',
          // Generate the 4 slides required
          range: [0, 3],
          rangeContent: function() {
            return '<div class="ms-slide__image-container"><div class="ms-slide__image"></div></div>';
          },
          // Syncronize the other sliders
          sync: [msNumbers, msTitles, msLinks],
          // Styles to interpolate as we move the slider
          style: {
            '.ms-slide__image': {
              transform: [{
                scale: [1.5, 1]
              }]
            }
          },


          // Update pagination if slider change
          change: function(newIndex, oldIndex) {
            if (typeof oldIndex !== 'undefined') {
              paginationItems[oldIndex].classList.remove('pagination__item--active');
            }
            paginationItems[newIndex].classList.add('pagination__item--active');
          }
        });


        // Select corresponding slider item when a pagination button is clicked
        pagination.addEventListener('click', function(e) {
          if (e.target.matches('.pagination__button')) {
            var index = paginationItems.indexOf(e.target.parentNode);
            msImages.select(index);
          }
        });

      })();
    </script>
    <!-- // for banner slider -->

    <!--  bootstrap js -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!--  //bootstrap js -->

    <!-- disable body scroll which navbar is in active -->
    <script>
      $(function() {
        $('.navbar-toggler').click(function() {
          $('body').toggleClass('noscroll');
        })
      });
    </script>
    <!-- disable body scroll which navbar is in active -->

  </body>

  <!-- Mirrored from demo.w3layouts.com/demos_new/template_demo/18-12-2019/eccentricportfolio-liberty-demo_Free/810562296/web/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 17 May 2020 12:07:24 GMT -->

</html>