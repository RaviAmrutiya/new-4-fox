<!--
Author: W3layouts
Author URL: http://w3layouts.com
-->
<!doctype html>
<html lang="en">


<!-- Mirrored from demo.w3layouts.com/demos_new/template_demo/18-12-2019/eccentricportfolio-liberty-demo_Free/810562296/web/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 17 May 2020 12:06:09 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Eccentric portfolio - personal website template | About : W3layouts</title>

    <!-- google fonts -->
    <!-- <link href="../../../../../../../fonts.googleapis.com/css07c1.css?family=Nunito:400,700&amp;display=swap" rel="stylesheet"> -->

    <!-- Template CSS -->
    <link rel="stylesheet" href="assets/css/style-liberty.css">

</head>

<body>
    <!-- <script src='../../../../../../../ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script><script src="../../../../../../../m.servedby-buysellads.com/monetization.js" type="text/javascript"></script> -->
    <script>
        (function() {
            if (typeof _bsa !== 'undefined' && _bsa) {
                // format, zoneKey, segment:value, options
                _bsa.init('flexbar', 'CKYI627U', 'placement:w3layoutscom');
            }
        })();
    </script>
    <script>
        (function() {
            if (typeof _bsa !== 'undefined' && _bsa) {
                // format, zoneKey, segment:value, options
                _bsa.init('fancybar', 'CKYDL2JN', 'placement:demo');
            }
        })();
    </script>
    <script>
        (function() {
            if (typeof _bsa !== 'undefined' && _bsa) {
                // format, zoneKey, segment:value, options
                _bsa.init('stickybox', 'CKYI653J', 'placement:w3layoutscom');
            }
        })();
    </script>
    <!--<script>(function(v,d,o,ai){ai=d.createElement("script");ai.defer=true;ai.async=true;ai.src=v.location.protocol+o;d.head.appendChild(ai);})(window, document, "//a.vdo.ai/core/w3layouts_V2/vdo.ai.js?vdo=34");</script>-->
    <div id="codefund">
        <!-- fallback content -->
    </div>
    <!-- <script src="../../../../../../../codefund.io/properties/441/funder.js" async="async"></script> -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src='https://www.googletagmanager.com/gtag/js?id=UA-149859901-1'></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-149859901-1');
    </script>

    <script>
        window.ga = window.ga || function() {
            (ga.q = ga.q || []).push(arguments)
        };
        ga.l = +new Date;
        ga('create', 'UA-149859901-1', 'demo.w3layouts.com');
        ga('require', 'eventTracker');
        ga('require', 'outboundLinkTracker');
        ga('require', 'urlChangeTracker');
        ga('send', 'pageview');
    </script>
    <!-- <script async src='../../../../../../js/autotrack.js'></script> -->

    <meta name="robots" content="noindex">

    <body>
        <!-- <link rel="stylesheet" href="../../../../../../images/demobar_w3_4thDec2019.css"> -->
        <!-- Demo bar start -->
        <!-- <div id="w3lDemoBar" class="w3l-demo-bar">
  <a href="https://w3layouts.com/?p=38444" ga-on="click" ga-event-category="Eccentric Portfolio Template" ga-event-action="view" ga-event-label="Eccentric Portfolio - view">
    <span class="w3l-icon -back">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path opacity=".87" fill="none" d="M0 0h24v24H0V0z"/><path d="M16.62 2.99c-.49-.49-1.28-.49-1.77 0L6.54 11.3c-.39.39-.39 1.02 0 1.41l8.31 8.31c.49.49 1.28.49 1.77 0s.49-1.28 0-1.77L9.38 12l7.25-7.25c.48-.48.48-1.28-.01-1.76z"/></svg>
    </span>
    <span class="w3l-text">Back</span>
  </a>
  <a href="https://w3layouts.com/" class="w3l-logo">W3layouts</a>
  <div class="w3l-template-options">
    <a href="https://w3layouts.com/?p=38444"
      class="w3l-download" ga-on="click" ga-event-category="Eccentric Portfolio Template" ga-event-action="download-options" ga-event-label="Eccentric Portfolio - Download options" title="Free download requires Backlink">
      <span class="w3l-icon -download">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M19 12v7H5v-7H3v7c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2v-7h-2zm-6 .67l2.59-2.58L17 11.5l-5 5-5-5 1.41-1.41L11 12.67V3h2z"/><path fill="none" d="M0 0h24v24H0z"/></svg>
      </span>
      <span class="w3l-text">Download</span>
    </a>
    <a href="https://w3layouts.com/?p=38444"
      class="w3l-buy" ga-on="click" ga-event-category="Eccentric Portfolio Template" ga-event-action="Buy Now" ga-event-label="Eccentric Portfolio - Buy" title="Remove Backlink from template">
      <span class="w3l-icon -buy">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M7 18c-1.1 0-1.99.9-1.99 2S5.9 22 7 22s2-.9 2-2-.9-2-2-2zM1 2v2h2l3.6 7.59-1.35 2.45c-.16.28-.25.61-.25.96 0 1.1.9 2 2 2h12v-2H7.42c-.14 0-.25-.11-.25-.25l.03-.12.9-1.63h7.45c.75 0 1.41-.41 1.75-1.03l3.58-6.49c.08-.14.12-.31.12-.48 0-.55-.45-1-1-1H5.21l-.94-2H1zm16 16c-1.1 0-1.99.9-1.99 2s.89 2 1.99 2 2-.9 2-2-.9-2-2-2z"/><path d="M0 0h24v24H0z" fill="none"/></svg>
      </span>
      <span class="w3l-text">Buy Now</span>
    </a>
  </div>
</div> -->

        <!-- inner page header -->
        <?php include('header.php'); ?>
        <!-- //inner page header-->

        <!-- about me -->
        <div class="w3l-about py-5">
            <div class="container py-lg-3">
                <div class="row about-content mb-lg-5">
                    <div class="col-lg-6 pr-md-5">
                        <div class="image-block">
                            <img src="assets/images/me.png" class="img-fluid myphoto" alt="my photo" />
                            <!-- <img src="assets/images/cross.png" class="img-fluid pos" alt="dots" /> -->
                        </div>
                    </div>
                    <div class="col-lg-6 info pl-lg-5 mt-lg-0 mt-5 pt-lg-0 pt-3 align-center">
                        <h4 class="">YOUR DREAMS OUR PASSION</h4>
                        <p class="mt-md-4 mt-3 mb-0">4FOX WEB SOLUTION is an IT company started in 2018 by group of people willing to work for the betterment of people and to give some contribution to the country.Our main purpose is to provide service and help clients in growing thire bussniess.</p>
                        <!-- <img src="assets/images/signature.png" class="img-fluid signature" width="300px" alt="my photo" /> -->

                    </div>
                </div>
            </div>
        </div>
        <!-- //about me -->

        <div class="display-ad" style="margin: 8px auto; display: block; text-align:center;">
            <!---728x90--->

        </div>
        <!-- stats -->
        <section class="w3l-stats py-lg-5 py-4" id="stats">
            <div class="gallery-inner container py-md-5 py-4">
                <div class="row stats-con text-white">
                    <div class="col-sm-3 col-6 stats_info counter_grid">
                        <span class="fa fa-lightbulb-o"></span>
                        <p class="counter">20</p>
                        <h4>Projects Done</h4>
                    </div>
                    <div class="col-sm-3 col-6 stats_info counter_grid1">
                        <span class="fa fa-heart"></span>
                        <p class="counter">1000</p>
                        <h4>Designs</h4>
                    </div>
                    <div class="col-sm-3 col-6 stats_info counter_grid mt-sm-0 mt-5">
                        <span class="fa fa-magic"></span>
                        <p class="counter">10290</p>
                        <h4>Lines Of Codes</h4>
                    </div>
                    <div class="col-sm-3 col-6 stats_info counter_grid2 mt-sm-0 mt-5">
                        <span class="fa fa-smile-o"></span>
                        <p class="counter">16</p>
                        <h4>Happy Clients</h4>
                    </div>
                </div>
            </div>
        </section>
        <!-- //stats -->

        <div class="display-ad" style="margin: 8px auto; display: block; text-align:center;">
            <!---728x90--->
        </div>
        <!-- about my profile -->
        <section class="w3l-about-bottom py-5" id="about">
            <div class="container py-lg-5 py-md-3">
                <div class="row middle-grids">
                    <div class="col-lg-7 advantage-grid-info">
                        <div class="advantage_left">
                            <h4>About Us </h4>
                            <p class="">We design and build CREATIVE WEBSITES, ANDROID APPLICATIONS, IOS APPLICATIONS, SOFTWARE ,
                                SOFTWARE AS SERVICE. We also provide you with other services like DIGITAL MARKETING, LOGO DESIGNING, SOCIAL MEDIA MARKETING, SEARCH ENGINE OPTIMIZATION, ENTERPRISE RESOURCE PLANNING(ERP).
                            </p>
                            <p class="mb-0">We are passionate about doing the work of our clients with the latest technology and make thire products more creative and effective.
                            We endow with various solutions from ranging small busniess to the international one.Our solution are simple and object oreinted across all segments.
                            </p>
                            <!-- <a href="resume.html" class="primary-btn-style btn-primary btn mt-lg-5 mt-4">Download CV</a>
                            <a href="contact.html" class="secondary-btn-style btn-secondary btn mt-lg-5 mt-4 ml-1">Hire Me</a> -->
                        </div>
                    </div>
                    <div class="col-lg-5 advantage-grid-info1">
                        <div class="advantage_left1 mt-lg-0 mt-5">
                            <img src="assets/images/about.jpg" class="img-fluid" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- //about my profile -->

        <div class="display-ad" style="margin: 8px auto; display: block; text-align:center;">
            <!---728x90--->
        </div>
        <!-- skills and hobbies -->
        <div class="w3l-skills py-5" id="skills">
            <div class="container py-lg-5 py-md-3">
                <div class="modal-spa">
                <center><b><h3 class="headingmagin">Our Work Profiles</h3></b></center>

                    <div class="row skills">
                        <div class="col-lg-6 bar-grids bargrids-left">
                            <!-- <h4> My Skills </h4> -->
                            <h6>creative web design <span> 98% </span></h6>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped active bg-primary" style="width: 98%">
                                </div>
                            </div>
                            <h6>ui design & developer <span> 80% </span></h6>
                            <div class="progress  w3-bar-grids">
                                <div class="progress-bar progress-bar-striped active" style="width: 80%">
                                </div>
                            </div>
                            <h6>Javascript<span>90% </span></h6>
                            <div class="progress  w3-bar-grids">
                                <div class="progress-bar progress-bar-striped active" style="width: 90%">
                                </div>
                            </div>
                            <h6>Photoshop <span> 75% </span></h6>
                            <div class="progress  w3-bar-grids prgs-w3-last">
                                <div class="progress-bar progress-bar-striped active" style="width: 75%">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 bar-grids bargrids-left mt-lg-0 mt-5">
                            <!-- <h4> My Skills </h4> -->
                            <h6>Android App Devlopment<span> 80% </span></h6>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped active bg-primary" style="width: 80%">
                                </div>
                            </div>
                            <h6>Ios App Devlopment<span> 70% </span></h6>
                            <div class="progress  w3-bar-grids">
                                <div class="progress-bar progress-bar-striped active" style="width: 70%">
                                </div>
                            </div>
                            <h6>Social media marketing<span>85% </span></h6>
                            <div class="progress  w3-bar-grids">
                                <div class="progress-bar progress-bar-striped active" style="width: 85%">
                                </div>
                            </div>
                            <h6>Logo Designing <span> 75% </span></h6>
                            <div class="progress  w3-bar-grids prgs-w3-last">
                                <div class="progress-bar progress-bar-striped active" style="width: 75%">
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- //skills and hobbies -->

        <!-- my featured projects -->
        <section class="w3l-block py-5">
            <div class="container py-lg-3">
                <h3 class="title mb-md-5 mb-4">Featured projects </h3>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 content" data-id="id-1" data-type="cat-item-1">
                        <span class="image-block">
                            <div class="content-overlay"></div>
                            <a class="image-zoom" href="#img">
                                <img src="assets/images/alexandra.jpg" class="img-fluid w3layouts agileits" alt="portfolio-img">
                                <div class="content-details fadeIn-bottom">
                                    <h3 class="content-title">This is a title</h3>
                                    <p class="content-text">This is a short description</p>
                                </div>
                            </a>
                        </span>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 content mt-sm-0 mt-4" data-id="id-2" data-type="cat-item-2">
                        <span class="image-block">
                            <div class="content-overlay"></div>
                            <a class="image-zoom" href="#img">
                                <img src="assets/images/bench.jpg" class="img-fluid w3layouts agileits" alt="portfolio-img">
                                <div class="content-details fadeIn-bottom">
                                    <h3 class="content-title">This is a title</h3>
                                    <p class="content-text">This is a short description</p>
                                </div>
                            </a>
                        </span>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 content mt-md-0 mt-4" data-id="id-2" data-type="cat-item-2">
                        <span class="image-block">
                            <div class="content-overlay"></div>
                            <a class="image-zoom" href="#img">
                                <img src="assets/images/alexandra.jpg" class="img-fluid w3layouts agileits" alt="portfolio-img">
                                <div class="content-details fadeIn-bottom">
                                    <h3 class="content-title">This is a title</h3>
                                    <p class="content-text">This is a short description</p>
                                </div>
                            </a>
                        </span>
                    </div>
                    <div class="content text-center mt-sm-5 mt-4">
                        <a href="portfolio.html" class="btn btn-primary primary-btn-style">View more</a>
                    </div>
                </div>
            </div>
        </section>
        <!-- //my featured projects -->

        <!-- Footer -->
        <?php include('footer.php'); ?>
        <!-- //Footer -->

        <!-- move top -->
        <button onclick="topFunction()" id="movetop" class="bg-primary" title="Go to top">
            <span class="fa fa-angle-up"></span>
        </button>
        <script>
            // When the user scrolls down 20px from the top of the document, show the button
            window.onscroll = function() {
                scrollFunction()
            };

            function scrollFunction() {
                if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                    document.getElementById("movetop").style.display = "block";
                } else {
                    document.getElementById("movetop").style.display = "none";
                }
            }

            // When the user clicks on the button, scroll to the top of the document
            function topFunction() {
                document.body.scrollTop = 0;
                document.documentElement.scrollTop = 0;
            }
        </script>
        <!-- /move top -->

        <!-- common jquery -->
        <script src="assets/js/jquery-3.3.1.min.js"></script>
        <!-- //common jquery -->

        <!-- disable body scroll which navbar is in active -->
        <script>
            $(function() {
                $('.navbar-toggler').click(function() {
                    $('body').toggleClass('noscroll');
                })
            });
        </script>
        <!-- disable body scroll which navbar is in active -->

        <!-- stats number counter-->
        <script src="assets/js/jquery.waypoints.min.js"></script>
        <script src="assets/js/jquery.countup.js"></script>
        <script>
            $('.counter').countUp();
        </script>
        <!-- //stats number counter -->

        <!--  bootstrap js -->
        <script src="assets/js/bootstrap.min.js"></script>
        <!--  //bootstrap js -->

    </body>

    <!-- Mirrored from demo.w3layouts.com/demos_new/template_demo/18-12-2019/eccentricportfolio-liberty-demo_Free/810562296/web/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 17 May 2020 12:06:42 GMT -->

</html>