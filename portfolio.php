<!--
Author: W3layouts
Author URL: http://w3layouts.com
-->
<!doctype html>
<html lang="en">


<!-- Mirrored from demo.w3layouts.com/demos_new/template_demo/18-12-2019/eccentricportfolio-liberty-demo_Free/810562296/web/services.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 17 May 2020 12:07:35 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Eccentric portfolio - personal website template | Services : W3layouts</title>

  <!-- google fonts -->
  <!-- <link href="../../../../../../../fonts.googleapis.com/css07c1.css?family=Nunito:400,700&amp;display=swap" rel="stylesheet"> -->

  <!-- Template CSS -->
  <link rel="stylesheet" href="assets/css/style-liberty.css">

</head>

<body>
<!-- <script src='../../../../../../../ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script><script src="../../../../../../../m.servedby-buysellads.com/monetization.js" type="text/javascript"></script> -->
<script>
(function(){
	if(typeof _bsa !== 'undefined' && _bsa) {
  		// format, zoneKey, segment:value, options
  		_bsa.init('flexbar', 'CKYI627U', 'placement:w3layoutscom');
  	}
})();
</script>
<script>
(function(){
if(typeof _bsa !== 'undefined' && _bsa) {
	// format, zoneKey, segment:value, options
	_bsa.init('fancybar', 'CKYDL2JN', 'placement:demo');
}
})();
</script>
<script>
(function(){
	if(typeof _bsa !== 'undefined' && _bsa) {
  		// format, zoneKey, segment:value, options
  		_bsa.init('stickybox', 'CKYI653J', 'placement:w3layoutscom');
  	}
})();
</script>
<!--<script>(function(v,d,o,ai){ai=d.createElement("script");ai.defer=true;ai.async=true;ai.src=v.location.protocol+o;d.head.appendChild(ai);})(window, document, "//a.vdo.ai/core/w3layouts_V2/vdo.ai.js?vdo=34");</script>-->
<div id="codefund"><!-- fallback content --></div>
<!-- <script src="../../../../../../../codefund.io/properties/441/funder.js" async="async"></script> -->
	
<!-- Global site tag (gtag.js) - Google Analytics -->
<!-- <script async src='https://www.googletagmanager.com/gtag/js?id=UA-149859901-1'></script> -->
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-149859901-1');
</script>

<script>
     window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
     ga('create', 'UA-149859901-1', 'demo.w3layouts.com');
     ga('require', 'eventTracker');
     ga('require', 'outboundLinkTracker');
     ga('require', 'urlChangeTracker');
     ga('send', 'pageview');
   </script>
<!-- <script async src='../../../../../../js/autotrack.js'></script> -->

<meta name="robots" content="noindex">
<!-- <body><link rel="stylesheet" href="../../../../../../images/demobar_w3_4thDec2019.css"> -->
	<!-- Demo bar start -->
 

<!-- inner page header -->

<!-- //inner page header-->
<?php include('header.php'); ?>
<!-- services block1 -->
<!-- <div class="w3-services py-5" id="services">
    <div class="container py-lg-3">
        <div class="title-section">
            <h3 class="main-title-w3">Our Services</h3>
            <div class="title-line">
            </div>
        </div>
        <div class="row w3-services-grids mt-lg-5 mt-4">
            <div class="col-lg-5 w3-services-left-grid">
                <h4>What i do</h4>
                <p>Lorem ipsum dolor sit amet, init sed adipisci ngelit. In a et euismod faucibus quam, a sodales er
                    osplacerat vitae. Sed pretium fermentum luctus.Cras sodales nisl vitae dolor facilisis dapibus. 
                    Integer consectetur in velit eget viverra. Quisque vulputate a nisi blandit molestie. Aenean sit 
                    amet consequat risus, eget egestas est.Nullam eu turpis diam. Ut ac erat vestibulum, laoreet ex 
                    faucibus, iaculis ex. Donec at dolor volutpat, laoreet nisi. </p>
                <div class="more">
                    <a href="#more" class="btn-primary btn primary-btn-style mt-lg-5 mt-4">Know more</a>
                </div>
            </div>
            <div class="col-lg-7 w3-services-right-grid mt-lg-0 mt-5 pl-lg-5">
                <div class="row w3-icon-grid-gap1">
                    <div class="col-md-6 col-sm-6 w3-icon-grid1">
                        <a href="#link">
                            <span class="fa fa-codepen text-primary" aria-hidden="true"></span>
                            <h3>Web design</h3>
                            <div class="clearfix"></div>
                        </a>
                        <p>Lorem ipsum dolor sit amet, init sed adipisci ngelit. In euismod faucibus.</p>
                    </div>
                    <div class="col-md-6 col-sm-6 w3-icon-grid1">
                        <a href="#link">
                            <span class="fa fa-mobile text-primary" aria-hidden="true"></span>
                            <h3>Mobile Apps</h3>
                            <div class="clearfix"></div>
                        </a>
                        <p>Lorem ipsum dolor sit amet, init sed adipisci ngelit. In euismod faucibus.</p>
                    </div>
                    <div class="col-md-6 col-sm-6 w3-icon-grid1">
                        <a href="#link">
                            <span class="fa fa-hourglass text-primary" aria-hidden="true"></span>
                            <h3>Animation</h3>
                            <div class="clearfix"></div>
                        </a>
                        <p>Lorem ipsum dolor sit amet, init sed adipisci ngelit. In euismod faucibus.</p>
                    </div>
                    <div class="col-md-6 col-sm-6 w3-icon-grid1">
                        <a href="#link">
                            <span class="fa fa-modx text-primary" aria-hidden="true"></span>
                            <h3>Photoshop</h3>
                            <div class="clearfix"></div>
                        </a>
                        <p>Lorem ipsum dolor sit amet, init sed adipisci ngelit. In euismod faucibus.</p>
                    </div>
                    <div class="col-md-6 col-sm-6 w3-icon-grid1">
                        <a href="#link">
                            <span class="fa fa-bar-chart text-primary" aria-hidden="true"></span>
                            <h3>Marketing</h3>
                            <div class="clearfix"></div>
                        </a>
                        <p>Lorem ipsum dolor sit amet, init sed adipisci ngelit. In euismod faucibus.</p>
                    </div>
                    <div class="col-md-6 col-sm-6 w3-icon-grid1">
                        <a href="#link">
                            <span class="fa fa-shopping-bag text-primary" aria-hidden="true"></span>
                            <h3>Development</h3>
                            <div class="clearfix"></div>
                        </a>
                        <p>Lorem ipsum dolor sit amet, init sed adipisci ngelit. In euismod faucibus.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->
<!-- //services block1 -->

<!-- <div class="display-ad" style="margin: 8px auto; display: block; text-align:center;"> -->
    <!---728x90--->

<!-- </div>s -->
<!-- services block 2 -->
<div class="display-ad" style="margin-top: 40px ; display: block; text-align:center;">

<h3  class="serviceHead">Our Clients</h3>

<div class="w3l-services-block py-5" id="classes">

    <div class="container py-lg-5 py-md-3">
        <div class="services-block_grids_bottom">

            <div class="row">
               
                <div class="col-lg-4 col-md-6 service_grid_btm_left">
                    <div class="service_grid_btm_left1">
                        <div class="service_grid_btm_left2">
                            <h5>Samir Kotak Chief Advisor</h5>
                        </div>
                        <div class="title-line">
                        </div>
                        <img src="assets/images/lic.png" alt=" " class="img-fluid" />
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 service_grid_btm_left mt-md-0 mt-sm-5 mt-4">
                    <div class="service_grid_btm_left1">
                        <div class="service_grid_btm_left2">
                            <h5>Aariya Polypack</h5>
                        </div>
                        <div class="title-line">
                        </div>
                        <img src="assets/images/Aariya.jpg" alt=" " class="img-fluid" />
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 service_grid_btm_left mt-lg-0 mt-sm-5 mt-4">
                    <div class="service_grid_btm_left1">
                        <div class="service_grid_btm_left2">
                            <!-- <span class="fa fa-apple text-primary" aria-hidden="true"></span> -->
                            <h5>Sanvi Spinning Mill</h5>
                            <!-- <p>Maecenas sodales eu velit in varius. vitae sem vitae urna tempus commodo.</p> -->
                        </div>
                        <div class="title-line">
                        </div>
                        <img src="assets/images/sanvi.jpg" alt=" " class="img-fluid" />
                    </div>
                </div>
            </div><br>
            <div class="row">
            <div class="col-lg-4 col-md-6 service_grid_btm_left">
                    <div class="service_grid_btm_left1">
                        <div class="service_grid_btm_left2">
                            <h5>Bond Chem Industries</h5>
                        </div>
                        <div class="title-line">
                        </div>
                        <img src="assets/images/bond_chem.jpg" alt=" " class="img-fluid" />
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 service_grid_btm_left mt-md-0 mt-sm-5 mt-4">
                    <div class="service_grid_btm_left1">
                        <div class="service_grid_btm_left2">
                            <h5>Donec Tiles</h5>
                        </div>
                        <div class="title-line">
                        </div>
                        <img src="assets/images/donec.jpg" alt=" " class="img-fluid" />
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 service_grid_btm_left mt-lg-0 mt-sm-5 mt-4">
                    <div class="service_grid_btm_left1">
                        <div class="service_grid_btm_left2">
                            <!-- <span class="fa fa-apple text-primary" aria-hidden="true"></span> -->
                            <h5>Sun Uma Ceramic</h5>
                            <!-- <p>Maecenas sodales eu velit in varius. vitae sem vitae urna tempus commodo.</p> -->
                        </div>
                        <div class="title-line">
                        </div>
                        <img src="assets/images/sun_uma.jpg" alt=" " class="img-fluid" />
                    </div>
                </div>
            </div><br>
            <div class="row">
                <div class="col-lg-4 col-md-6 service_grid_btm_left">
                    <div class="service_grid_btm_left1">
                        <div class="service_grid_btm_left2">
                            <h5>Vishva Marketing</h5>
                        </div>
                        <div class="title-line">
                        </div>
                        <img src="assets/images/vishva.jpg" alt=" " class="img-fluid" />
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 service_grid_btm_left mt-md-0 mt-sm-5 mt-4">
                    <div class="service_grid_btm_left1">
                        <div class="service_grid_btm_left2">
                            <h5>Vecton Energy Tiles</h5>
                        </div>
                        <div class="title-line">
                        </div>
                        <img src="assets/images/vecton.jpg" alt=" " class="img-fluid" />
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 service_grid_btm_left mt-lg-0 mt-sm-5 mt-4">
                    <div class="service_grid_btm_left1">
                        <div class="service_grid_btm_left2">
                            <!-- <span class="fa fa-apple text-primary" aria-hidden="true"></span> -->
                            <h5>vanity House</h5>
                            <!-- <p>Maecenas sodales eu velit in varius. vitae sem vitae urna tempus commodo.</p> -->
                        </div>
                        <div class="title-line">
                        </div>
                        <img src="assets/images/vanity.jpg" alt=" " class="img-fluid" />
                    </div>
                </div>
            </div><br>
            
            
        </div>
        
    </div>
</div>
<!-- // services block2 -->

<!-- <div class="display-ad" style="margin: 8px auto; display: block; text-align:center;"> -->
    <!---728x90--->
</div>


    <!---728x90--->
</div>
<!-- services block4 -->

<!-- //services block4 -->

<!-- Footer -->
<section class="w3l-footers-1">
	<div class="footer bg-secondary">
		<div class="container">
			<div class="footer-content">
				<div class="row">
					<div class="col-lg-8 footer-left">
						<p class="m-0">© Copyright 2020 Eccentric Portfolio. Design by <a
								href="https://w3layouts.com/">W3layouts</a></p>
					</div>
					<div class="col-lg-4 footer-right text-lg-right text-center mt-lg-0 mt-3">
						<ul class="social m-0 p-0">
							<li><a href="#facebook"><span class="fa fa-facebook"></span></a></li>
							<li><a href="#linkedin"><span class="fa fa-linkedin"></span></a></li>
							<li><a href="#instagram"><span class="fa fa-instagram"></span></a></li>
							<li><a href="#twitter"><span class="fa fa-twitter"></span></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- //Footer -->

<!-- move top -->
<button onclick="topFunction()" id="movetop" class="bg-primary" title="Go to top">
	<span class="fa fa-angle-up"></span>
</button>
<script>
	// When the user scrolls down 20px from the top of the document, show the button
	window.onscroll = function () {
		scrollFunction()
	};

	function scrollFunction() {
		if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
			document.getElementById("movetop").style.display = "block";
		} else {
			document.getElementById("movetop").style.display = "none";
		}
	}

	// When the user clicks on the button, scroll to the top of the document
	function topFunction() {
		document.body.scrollTop = 0;
		document.documentElement.scrollTop = 0;
	}
</script>
<!-- /move top -->

<!-- common jquery -->
<script src="assets/js/jquery-3.3.1.min.js"></script>
<!-- //common jquery -->

<!-- disable body scroll which navbar is in active -->
<script>
  $(function () {
    $('.navbar-toggler').click(function () {
      $('body').toggleClass('noscroll');
    })
  });
</script>
<!-- disable body scroll which navbar is in active -->

<!--  bootstrap js -->
<script src="assets/js/bootstrap.min.js"></script>
<!--  //bootstrap js -->

</body>

<!-- Mirrored from demo.w3layouts.com/demos_new/template_demo/18-12-2019/eccentricportfolio-liberty-demo_Free/810562296/web/services.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 17 May 2020 12:07:36 GMT -->
</html>